#pragma once
#include <memory>
#include <string>
#include "Package.h"
#include "Sync.h"

class Ialgorithm;

class Peer
{
public:
	unsigned long long int calculate(unsigned int n);

	unsigned int getIP() { return IP; }
	void operator()(Sync & sync);

	Peer(const std::string & method);
	Peer(Peer && rhs);
	Peer& operator=(Peer && rhs);
	~Peer();

protected:
	std::unique_ptr<Ialgorithm> alg;

private:
	unsigned int IP = 0;
};
