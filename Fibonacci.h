#pragma once
#include "Ialgorithm.h"

class Fibonacci :
	public Ialgorithm
{
public:
	virtual unsigned long long int calculate(unsigned int n) override;

	Fibonacci();
	virtual ~Fibonacci();
};
