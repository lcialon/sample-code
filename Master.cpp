#include <iostream>
#include <mutex>
#include "Master.h"
using namespace std::chrono_literals;

void Master::operator()(Sync & sync)
{

	while (true)
	{
		std::unique_lock<std::mutex> locker(sync.mut);
		if (sync.cond.wait_for(locker, 1s, [&]() { return !sync.deq.empty(); }))
		{
			auto tempPack = std::move(sync.deq.back());
			sync.deq.pop_back();
			std::cout << "Master received from " << std::hex << unpackIP(tempPack) << std::dec << " value " << unpackValue(tempPack) << std::endl;
			locker.unlock();
		}
		else
		{
			break;
		}
	}
}


Master::Master()
{
}


Master::~Master()
{
}
