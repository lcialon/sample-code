#include "Factorial.h"

unsigned long long int Factorial::calculate(unsigned int n)
{
	auto it = results.find(n);
	if (it != results.end())
	{
		return it->second;
	}
	return ((results.insert({ n, calculate(n - 1) * n})).first)->second;
}


Factorial::Factorial()
{
	results = { { 0,1 }, { 1,1 } };
}


Factorial::~Factorial()
{
}
