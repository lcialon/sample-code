#pragma once
#include <unordered_map>

class Ialgorithm
{
public:
	virtual unsigned long long int calculate(unsigned int n) = 0;

	Ialgorithm();
	virtual ~Ialgorithm();

protected:
	std::unordered_map<unsigned int, unsigned long long int> results;
};
