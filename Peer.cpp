#include <iostream>
#include <mutex>
#include <string>
#include <utility>
#include "Factorial.h"
#include "Fibonacci.h"
#include "Peer.h"
using namespace std::chrono_literals;

unsigned long long int Peer::calculate(unsigned int n)
{
	return alg->calculate(n);
}


Peer::Peer(const std::string & method)
{
	if ("Fibonacci" == method)
	{
		alg = std::make_unique<Fibonacci>();
		IP = 0x0101'0101;
	}
	else if ("Factorial" == method)
	{
		alg = std::make_unique<Factorial>();
		IP = 0x0101'0102;
	}
	else
	{
		throw std::exception();
	}
}


void Peer::operator()(Sync & sync)
{
	for (auto i = 0u; i < 20; ++i)
	{
		auto value = calculate(i);
		std::unique_lock<std::mutex> locker(sync.mut);
		std::cout << "Peer " << std::hex << IP << std::dec << " produced " << value << std::endl;
		sync.deq.emplace_front(value, IP);
		locker.unlock();
		sync.cond.notify_one();
		std::this_thread::sleep_for(50ms);
	}
}


Peer::Peer(Peer && rhs) 
	: alg{ std::exchange(rhs.alg, nullptr) }
	, IP(rhs.IP)
{  
}


Peer& Peer::operator=(Peer && rhs) 
{
	alg = std::exchange(rhs.alg, nullptr);
	IP = rhs.IP; 
	return *this;
}


Peer::~Peer()
{
}
