#pragma once
#include <deque>
#include <mutex>
#include "Package.h"

class Sync
{
public:
	std::deque<Package> deq;
	std::mutex mut;
	std::condition_variable cond;

	Sync();
	~Sync();
};
