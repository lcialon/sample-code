#pragma once
#include "Package.h"
#include "Sync.h"

class Master
{
public:
	unsigned long long int unpackValue(Package pack) { return pack.value; }
	unsigned int unpackIP(Package pack) { return pack.IPaddress; }

	void operator()(Sync & sync);

	Master();
	~Master();
};
