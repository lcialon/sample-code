#pragma once
#include "Ialgorithm.h"

class Factorial :
	public Ialgorithm
{
public:
	virtual unsigned long long int calculate(unsigned int n) override;

	Factorial();
	virtual ~Factorial();
};
