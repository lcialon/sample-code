/* Simple C++11/14 multithreaded program 
 * with two Peers which are sending some values 
 * and a Master which receives packages. 
 */

#include <memory>
#include <thread>
#include "Master.h"
#include "Peer.h"
#include "Sync.h"

int main()
{
	Sync sync;
	auto pPeer1 = std::make_unique<Peer>("Fibonacci");
	std::thread thPeer1(std::move(*pPeer1.release()), std::ref(sync));

	auto pPeer2 = std::make_unique<Peer>("Factorial");
	std::thread thPeer2(std::move(*pPeer2.release()), std::ref(sync));

	auto pMaster = std::make_unique<Master>();
	std::thread thMaster(std::move(*pMaster.release()), std::ref(sync));

	thPeer1.join();
	thPeer2.join();
	thMaster.join();

	return 0;
}
