#include "Fibonacci.h"

unsigned long long int Fibonacci::calculate(unsigned int n)
{
	auto it = results.find(n);
	if (it != results.end())
	{
		return it->second;
	}
	return ((results.insert({ n, calculate(n - 2) + calculate(n - 1) })).first)->second;
}


Fibonacci::Fibonacci()
{
	results = { { 0,0 }, { 1,1 } };
}


Fibonacci::~Fibonacci()
{
}
